Table of contents
-----------------

* Introduction
* Features
* Notes
* References


Introduction
------------

The **Schema.org Blueprints Starter Kit: Layout module** provides
Schema.org types required to build a pages using Layout Paragraphs 
with Mercury Editor.


Features
--------

**Installation**

- Adds field prefix and suffix support to the 'layout' component.
- Hides labels for all schema_* field in paragraph view displays.

**Paragraphs**

- Enables 'style_options' for all paragraph types used in layout paragraphs.
- Adds Slick carousel to galleries.
- Hides labels for all schema_* field in paragraph view displays.
- Loads a Thing of Thing's sameAs target entity and return its JSON-LD.

**Mercury Editor**

- Configure Mercury Editor's groups and default settings.

**Style Options**

- Defines 'Header tag' as a style option.
- Add field prefix and suffix to 'Header tags' as needed.
- Adds description to 'CSS class' style option.

**Default Content**

- **Style Guide: Basic Content**: Examples of all the basic components available using CKEditor.

- **Style Guide: Advanced Content** : Examples of all the page components available using Mercury Editor.


Schema.org types
----------------

Below are the Schema.org types created by this starter kit.

- **Basic Content** (paragraph:basic_content:WebContent)  
  Displays basic web content which includes title and text.  
  <https://schema.org/basic_content>

- **Advanced Content** (paragraph:advanced_content:WebContent)  
  Displays advanced web content which includes title, image, text, and link.  
  <https://schema.org/advanced_content>

- **Quotation** (paragraph:quotation:Quotation)  
  <https://schema.org/quotation>

- **Statement** (paragraph:statement:Statement)  
  <https://schema.org/statement>

- **Header** (paragraph:header:Statement)  
  A heading.  
  <https://schema.org/header>

- **Collection Page** (paragraph:collection_page:CollectionPage)  
  <https://schema.org/collection_page>

- **Media Gallery** (paragraph:media_gallery:MediaGallery)  
  <https://schema.org/media_gallery>

- **Image Gallery** (paragraph:image_gallery:ImageGallery)  
  <https://schema.org/image_gallery>

- **Video Gallery** (paragraph:video_gallery:VideoGallery)  
  <https://schema.org/video_gallery>

- **List (Rich text)** (paragraph:item_list_text:ItemList)  
  A list of rich text items.  
  <https://schema.org/item_list_text>

- **List (Plain text)** (paragraph:item_list_string:ItemList)  
  A list of plain text items.  
  <https://schema.org/item_list_string>

- **List (Links)** (paragraph:item_list_link:ItemList)  
  A list of links.  
  <https://schema.org/item_list_link>

- **List (Content)** (paragraph:item_list_node:ItemList)  
  A list of links to content.  
  <https://schema.org/item_list_node>

- **List (Paragraph)** (paragraph:item_list_paragraph:ItemList)  
  A list of web content displayed as an accordion or vertical tabs.  
  <https://schema.org/item_list_paragraph>

- **Custom block** (paragraph:block_content:Thing)  
  A reusable piece of content, such as a banner, call out, or a message.  
  <https://schema.org/block_content>

- **Media** (paragraph:media:Thing)  
  A video, image, audio file, or digital asset.  
  <https://schema.org/media>

- **Content** (paragraph:node:Thing)  
  A piece of individual content, such as a page, poll, article, forum topic, or a blog entry.   
  <https://schema.org/node>

- **Configurable block** (paragraph:block:Thing)  
  A configurable piece of your site's web page layout.  
  <https://schema.org/block>

- **View** (paragraph:view:Thing)  
  A view is a listing of content on a website.   
  <https://schema.org/view>

- **Webform** (paragraph:webform:Thing)  
  A form to collect information or ask questions.  
  <https://schema.org/webform>

- **Basic page** (node:page:WebPage)  
  A web page.  
  <https://schema.org/page>

- **Advanced page** (node:advanced_page:WebPage)  
  A web page.  
  <https://schema.org/advanced_page>


Requirements
------------

**[Layout Paragraphs](https://www.drupal.org/project/layout_paragraphs)**  
Field widget and formatter for using layouts with paragraph fields.

**[Mercury Editor](https://www.drupal.org/project/mercury_editor)**  
Effortless, drag-and-drop editing for Drupal.

**[Style Options](https://www.drupal.org/project/style_options)**   
Provides configurable styles management for attaching various style plugins to Layouts and Paragraphs.

**[Slick](http://kenwheeler.github.io/slick/)**  
The last carousel you'll ever need


Development
-----------

## [Default Content](https://www.drupal.org/project/default_content)

[Defining default content](https://www.drupal.org/docs/contributed-modules/default-content-for-d8/defining-default-content)

```bash
# Collect default content uuids. 
echo 'node:'; ddev drush sqlq "SELECT CONCAT('- ', uuid) FROM node WHERE nid = 1";
echo 'media:'; ddev drush sqlq "SELECT CONCAT('- ', uuid) FROM media";
echo 'file:'; ddev drush sqlq "SELECT CONCAT('- ', uuid) FROM file_managed WHERE filename LIKE 'generateImage_%'";

# Export default content.
ddev drush default-content:export-module schemadotorg_starterkit_layout;
```

## Development

**Rebuild**
```
# OPTION 1: Fresh site install and then install the starter kit.
ddev install admin; ddev drush en -y schemadotorg_starterkit_layout

# OPTION 2: Database back and restore and then install the starter kit.

# Export database after fresh site install.
ddev install admin; ddev export-db --gzip=false --file=database.sql;

# Import database and install layout.
ddev import-db --file=database.sql; ddev drush en -y schemadotorg_starterkit_layout;
```
