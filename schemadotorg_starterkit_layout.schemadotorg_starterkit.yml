types:

  # Layout.

  paragraph:section:WebContent:
    entity:
      label: Section
      behavior_plugins:
        layout_paragraphs:
          enabled: true
          available_layouts:
            layout_onecol: 'One column'
        style_options:
          enabled: true
    properties:
      name: true
      text: true
      image: false
      about: false

  paragraph:layout:WebContent:
    entity:
      label: Layout
      behavior_plugins:
        layout_paragraphs:
          enabled: true
          available_layouts:
            layout_onecol: layout_onecol
            layout_twocol_section: layout_twocol_section
            layout_twocol: layout_twocol
            layout_twocol_bricks: layout_twocol_bricks
            layout_threecol_section: layout_threecol_section
            layout_threecol_25_50_25: layout_threecol_25_50_25
            layout_threecol_33_34_33: layout_threecol_33_34_33
            layout_fourcol_section: layout_fourcol_section
        style_options:
          enabled: true
    properties:
      name: true
      text: true
      image: false
      about: false

  # Content.

  paragraph:basic_content:WebContent:
    entity:
      label: 'Basic Content'
      description: 'Displays basic web content which includes title and text.'
    properties:
      name: true
      text: true
      image: false
      about: false
  paragraph:advanced_content:WebContent:
    entity:
      label: 'Advanced Content'
      description: 'Displays advanced web content which includes title, image, text, and link.'
    properties:
      name: true
      image: true
      text: true
      about:
        widget_id: linkit_attributes
        widget_settings:
          linkit_auto_link_text: true
          enabled_attributes:
            id: false
            name: false
            target: false
            rel: false
            class: true
            accesskey: false

  # Statement.

  paragraph:statement:Statement: {  }
  paragraph:header:Statement:
    entity:
      label: Header
      description: 'A heading.'
    properties:
      headline: true
      text: false

  # Quotation.

  paragraph:quotation_person:Quotation:
    entity:
      label: 'Quotation (Person)'
    properties:
      creator:
        type: 'field_ui:entity_reference:node'
    additional_mappings:
      Person: false

  paragraph:quotation_custom:Quotation:
    entity:
      label: 'Quotation (Custom)'
    properties:
      creator: false
      text:
        widget_settings:
          weight: 1
        formatter_settings:
          weight: 1
    additional_mappings:
      Person:
        schema_properties:
          name:
            widget_settings:
              weight: 2
            formatter_settings:
              weight: 2
          jobTitle:
            widget_settings:
              weight: 3
            formatter_settings:
              weight: 3
          image:
            widget_settings:
              weight: 4
            formatter_settings:
              weight: 4

  # Collections / Galleries.

  paragraph:collection_page:CollectionPage:
    properties:
      name: true
      text: true
      hasPart:
        type: 'field_ui:entity_reference_revisions:paragraph'
        machine_name: has_part_paragraph
        label: Items
        required: true
  paragraph:media_gallery:MediaGallery:
    properties:
      name: true
      text: true
      hasPart:
        type: 'field_ui:entity_reference:media'
        machine_name: has_part_media
        label: Media
        required: true
  paragraph:image_gallery:ImageGallery:
    properties:
      name: true
      text: true
      hasPart:
        label: Images
        machine_name: has_part_media
        type: 'field_ui:entity_reference:media'
        required: true
  paragraph:video_gallery:VideoGallery:
    properties:
      name: true
      text: true
      hasPart:
        label: Videos
        machine_name: has_part_media
        type: 'field_ui:entity_reference:media'
        required: true

  # Item lists.

  paragraph:item_list_text:ItemList:
    entity:
      label: 'List (Rich text)'
      description: 'A list of rich text items.'
    properties:
      description:
        machine_name: text
      itemListElement:
        machine_name: item_text
        type: text_long
        required: true
  paragraph:item_list_string:ItemList:
    entity:
      label: 'List (Plain text)'
      description: 'A list of plain text items.'
    properties:
      description:
        machine_name: text
      itemListElement:
        machine_name: item_string
        type: string
        required: true
  paragraph:item_list_link:ItemList:
    entity:
      label: 'List (Links)'
      description: 'A list of links.'
    properties:
      description:
        machine_name: text
      itemListElement:
        machine_name: item_link
        type: link
        required: true
  paragraph:item_list_node:ItemList:
    entity:
      label: 'List (Content)'
      description: 'A list of links to content.'
    properties:
      description:
        machine_name: text
      itemListElement:
        machine_name: item_node
        type: field_ui:entity_reference:node
        label: Contents
        required: true
  paragraph:item_list_paragraph:ItemList:
    entity:
      label: 'List (Paragraph)'
      description: 'A list of web content displayed as an accordion or vertical tabs.'
    properties:
      description:
        machine_name: text
      itemListElement:
        machine_name: item_paragraph
        type: 'field_ui:entity_reference_revisions:paragraph'
        label: Contents
        required: true
        handler: 'default:paragraph'
        handler_settings:
          target_bundles:
            basic_content: basic_content
          negate: 0
          target_bundles_drag_drop:
            basic_content:
              weight: 0
              enabled: true
        widget_settings:
          edit_mode: closed
          autocollapse: all
  paragraph:item_list_custom:ItemList:
    entity:
      label: 'List (Custom field)'
      description: 'A list of custom field items.'
    properties:
      description:
        machine_name: text
      itemListElement:
        machine_name: item_custom
        required: true

  # Content entity references.
  paragraph:media:Thing:
    entity:
      label: Media
      description: 'A video, image, audio file, or digital asset.'
    properties:
      sameAs:
        machine_name: media
        label: Media
        type: field_ui:entity_reference:media
        required: true
        unlimited: false
        schema_types:
          MediaObject: MediaObject
  paragraph:node:Thing:
    entity:
      label: Content
      description: 'A piece of individual content, such as a page, poll, article, forum topic, or a blog entry. '
    properties:
      sameAs:
        machine_name: node
        label: Content
        type: field_ui:entity_reference:node
        required: true
        unlimited: false
        schema_types:
          Thing: Thing
        formatter_id: entity_reference_entity_view
        formatter_settings:
          view_mode: teaser
          link: false

  # Config entity references.

  paragraph:block:Thing:
    entity:
      label: 'Configurable block'
      description: 'A configurable piece of your site''s web page layout.'
    properties:
      sameAs:
        machine_name: block
        label: 'Configurable block'
        type: block_field
        required: true
        unlimited: false
  paragraph:view:Thing:
    entity:
      label: View
      description: 'A view is a listing of content on a website.'
    properties:
      sameAs:
        machine_name: view
        label: View
        type: viewsreference
        required: true
        unlimited: false
  paragraph:webform:Thing:
    entity:
      label: Webform
      description: 'A form to collect information or ask questions.'
    properties:
      sameAs:
        machine_name: webform
        label: Webform
        type: webform
        required: true
        unlimited: false
  paragraph:field:Thing:
    entity:
      label: 'Content field'
      description: 'A field on the main content.'
    properties:
      field_content_field:
        name: field_content_field
        type: list_string
        label: Field
        description: 'The field to display via the token or default display.'
        required: true
        allowed_values: {  }
        allowed_values_function: schemadotorg_starterkit_layout_content_field_allowed_values

  # Pages.

  node:page:WebPage:
    entity:
      label: 'Basic page'
  node:advanced_page:WebPage:
    entity:
      label: 'Advanced page'
    properties:
      field_hero:
        name: field_hero
        type: 'field_ui:entity_reference_revisions:paragraph'
        label: Hero
        group: false
        handler: 'default:paragraph'
        handler_settings:
          target_bundles:
            advanced_content: advanced_content
        widget_id: layout_paragraphs
        widget_settings:
          weight: 100
          empty_message: 'Click the [+] sign below to choose your hero component.'
        formatter_id: layout_paragraphs
        formatter_settings:
          weight: 100
          label: hidden
      mainEntity:
        widget_settings:
          weight: 101
          nesting_depth: 1
        formatter_settings:
          weight: 101
          label: hidden


