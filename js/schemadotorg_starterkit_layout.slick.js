/* eslint-disable strict */

/**
 * @file
 * Schema.org Blueprints Starter Kit: Layout slick behaviors.
 */

'use strict';

(($, Drupal, once) => {
  /**
   * Enhances paragraph components with a basic Slick carousel.
   *
   * @type {Drupal~behavior}
   *
   * @see schemadotorg_demo_admin_entity_view_alter()
   */
  Drupal.behaviors.schemaDotOrgStarterkitLayoutSlick = {
    attach: function attach(context) {
      once(
        'schemadotorg-starterkit-layout-slick',
        '.schemadotorg-starterkit-layout-slick',
        context,
      ).forEach((element) => {
        $(element).slick({
          slidesToShow: 1,
          dots: true,
          centerMode: true,
          autoplay: true,
          speed: 300,
        });
      });
    },
  };
})(jQuery, Drupal, once);
