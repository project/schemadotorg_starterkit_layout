<?php

declare(strict_types=1);

namespace Drupal\schemadotorg_starterkit_layout\Plugin\StyleOption;

use Drupal\Core\Form\FormStateInterface;
use Drupal\mercury_editor\Form\EditComponentForm;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\schemadotorg_field_parts\SchemaDotOrgFieldPartsManagerInterface;
use Drupal\style_options\Plugin\StyleOptionPluginBase;

/**
 * Define the Schema.org: Header plugin.
 *
 * @StyleOption(
 *   id = "schemadotorg_header",
 *   label = @Translation("Schema.org: Header"),
 * )
 */
class SchemaDotOrgHeader extends StyleOptionPluginBase {

  /**
   * Header field names.
   *
   * @var string[]
   */
  protected $fieldNames = [
    'schema_name',
    'schema_headline',
  ];

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    // Make sure the mercury editor component's paragraph
    // has a header field.
    $form_object = $form_state->getFormObject();
    if ($form_object instanceof EditComponentForm) {
      /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
      $paragraph = $form_object->getParagraph();
      if (!$this->hasField($paragraph)) {
        return $form;
      }
    }

    // Header tag.
    $form['tag'] = [
      '#type' => 'select',
      '#title' => $this->t('@label tag', ['@label' => $this->getLabel()]),
      '#description' => $this->getConfiguration('description'),
      '#options' => $this->getConfiguration('tags'),
      '#empty_option' => $this->t('- Default -'),
      '#default_value' => $this->getValue('tag') ?? '',
    ];

    // Header icon.
    $form['icon'] = [
      '#type' => 'select',
      '#title' => $this->t('@label icon', ['@label' => $this->getLabel()]),
      '#description' => $this->t('Select icon to be displayed before the header text'),
      '#options' => $this->getConfiguration('icons'),
      '#empty_option' => $this->t('- None -'),
      '#default_value' => $this->getValue('icon') ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function build(array $build): array {
    $header_tag = $this->getValue('tag') ?: 'h2';
    $header_icon = $this->getValue('icon') ?: '';

    /** @var \Drupal\paragraphs\ParagraphInterface|null $paragraph */
    $paragraph = $build['#paragraph'] ?? NULL;
    foreach ($this->fieldNames as $field_name) {
      if (!isset($build[$field_name])) {
        continue;
      }

      // Wrap header text in header tag.
      $build[$field_name][0]['#prefix'] = '<' . $header_tag . ' class="schemadotorg-header-text">';
      $build[$field_name][0]['#suffix'] = '</' . $header_tag . '>';

      // Add fontawesome icon to the header tag.
      if ($header_icon) {
        $build[$field_name][0]['#prefix'] .= '<span class="fas fa-' . $header_icon . '"></span> ';
      }

      // Add prefix and suffix values to the header.
      if ($paragraph) {
        foreach (SchemaDotOrgFieldPartsManagerInterface::PARTS as $part) {
          $part_field_name = $field_name . '_' . $part;
          if ($paragraph->hasField($part_field_name) && $paragraph->{$part_field_name}->value) {
            $part_html = '<div class="schemadotorg-header-' . $part . '">'
              . $paragraph->{$part_field_name}->value
              . '</div>';
            if ($part === SchemaDotOrgFieldPartsManagerInterface::PREFIX) {
              $build[$field_name][0]['#prefix'] = $part_html . $build[$field_name][0]['#prefix'];
            }
            else {
              $build[$field_name][0]['#suffix'] = $build[$field_name][0]['#suffix'] . $part_html;
            }

            // Hide field part if it is being rendered as a field.
            if (isset($build[$part_field_name])) {
              $build[$part_field_name]['#access'] = FALSE;
            }
          }
        }
      }

      // Wrap the entire header in container.
      $build[$field_name][0]['#prefix'] = '<div class="schemadotorg-header">'
        . $build[$field_name][0]['#prefix'];
      $build[$field_name][0]['#suffix'] = $build[$field_name][0]['#suffix']
        . '</div>';
    }
    return $build;
  }

  /**
   * Determine if the paragraph has a header field.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   A paragraph.
   *
   * @return bool
   *   TRUE if the paragraph has a header field.
   */
  protected function hasField(ParagraphInterface $paragraph): bool {
    foreach ($this->fieldNames as $field_name) {
      if ($paragraph->hasField($field_name)) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
